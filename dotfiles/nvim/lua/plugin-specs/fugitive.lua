return {
  "tpope/vim-fugitive",
  cmd="G",
  keys={
    {"<leader>gs", ":vertical Git <bar> exe 'vert resize ' &columns * 0.3 <CR>", silent=true},
    {"<leader>gd", ":Gdiffsplit!<CR>", silent=true},
    -- {"<leader>gD", ":GitReview<CR>", silent=true},
  },
}
