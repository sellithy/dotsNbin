
-- https://vim.fandom.com/wiki/Alternative_tab_navigation
vim.keymap.set("n", "tt", ":tabe<Space>")
vim.keymap.set("n", "tj", ":-tabm<CR>", {silent=true})
vim.keymap.set("n", "tk", ":+tabm<CR>", {silent=true})
vim.keymap.set("n", "tl", ":tabn<CR>", {silent=true})
vim.keymap.set("n", "th", ":tabp<CR>", {silent=true})
vim.keymap.set("n", "tn", ":tabnew<CR>", {silent=true})
vim.keymap.set("n", "tc", ":tabclose<CR>", {silent=true})

vim.opt.showtabline=2

-- https://stackoverflow.com/a/22614451
vim.opt.splitright=true
vim.opt.splitbelow=true
