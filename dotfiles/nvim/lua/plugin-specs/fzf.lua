return {
  "ibhagwan/fzf-lua",
  dependencies={"nvim-tree/nvim-web-devicons"},
  cmd='FzfLua',
  keys={
    {"<leader>sf", "<cmd>FzfLua files<CR>", silent=true},
    {"<leader>sF", ":FzfLua files cwd="},
    {"<leader>sr", "<cmd>FzfLua grep<CR>", silent=true},
    {"<leader>sR", "<cmd>FzfLua grep_last<CR>", silent=true},
    {"<leader>sw", "<cmd>FzfLua grep_cword<CR>", silent=true},
    {"<leader>sW", "<cmd>FzfLua grep_cWORD<CR>", silent=true},
  },
  config=function()
    local actions = require'fzf-lua.actions'
    require'fzf-lua'.setup{
      winopts={
        height=0.9,
        width=1,
        row=0.5,
        preview={
          vertical='up:75%',
          layout='vertical',
        },
      },
      actions={
        files={
          true,
          ["enter"]=actions.file_tabedit,
        },
      },
    }
  end
}
