#!/usr/bin/env bash


main() {
    local onlyLinks forceLinks
    args "$@"

    dotsNbinPath=$(dirname "$(dirname "$(realpath "$0")")")

    files='
        dotfiles/git/.gitconfig         .gitconfig
        dotfiles/git/.gitignore         .gitignore
        dotfiles/git/.gitmessage        .gitmessage

        dotfiles/.hushlogin             .hushlogin
        dotfiles/.tmux.conf             .tmux.conf

        dotfiles/zsh/zshenv             .zshenv
        dotfiles/zsh/zshrc              .zshrc

        dotfiles/nvim                   .config/nvim
    '

    echo "$files" | awk 'NF' | while read -r src dst; do
        handleLinking "$dotsNbinPath/$src" "$HOME/$dst" "$forceLinks"
    done

    $onlyLinks && exit

    handleBrew() {
        which brew &>/dev/null && { echo "Brew installed already :)">&2; return; }

        read -rsn1 -p "Brew not found. Install? (Y/n): "
        [[ -z $REPLY ]] && echo || echo "$REPLY"
        [[ $REPLY =~ ^[nN]$ ]] && return

        /usr/bin/env bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" || return 1
        eval "$(/opt/homebrew/bin/brew shellenv)"
        brew bundle --file "$dotsNbinPath/dotfiles/brew/Brewfile"

        echo
        echo "Brew installed zsh. You still need to make Brew's zsh the default shell using chsh."
        echo "https://gist.github.com/ngocphamm/4978435"
        echo
    }
    handleBrew

    handleSdkman() {
        [[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && { echo "Sdkman installed already :)">&2; return; }

        read -rsn1 -p "Sdkman not found. Install? (Y/n): "
        [[ -z $REPLY ]] && echo || echo "$REPLY"
        [[ $REPLY =~ ^[nN]$ ]] && return

        curl -s "https://get.sdkman.io?rcupdate=false" | bash
        source "$HOME/.sdkman/bin/sdkman-init.sh"
    }
    handleSdkman
}

handleLinking() {
    local src="$1"
    local dst="$2"
    local forceLinks="$3"

    local baseFileName; baseFileName="$(basename "$dst")"

    [[ -L "$dst" && $(readlink -n "$dst") == "$src" ]] && { echo "$baseFileName is already linked :)">&2; return; }
    ! $forceLinks && [[ -e "$dst" ]] && { echo "$baseFileName exists. Skipping">&2; return 1; }

    # shellcheck disable=SC2046
    mkdir -p "$(dirname "$dst")" && ln $($forceLinks && echo -f) -s "$src" "$dst" || return 1
    echo "$baseFileName Linked successfully :)">&2
}

args() {
    local usage; usage="usage: $(basename "$0") [-h | --help] [-l | --only-links] [-f | --force-links]"

    read -rd '' usage_full <<-EOV
	Sets up symlinks and installs needed apps (brew and sdkman)

	    only-links   Only do symlinks. Don't install anything else
	    force-links  Force overriding symlinks
	EOV

    onlyLinks='false' forceLinks='false'
    while [[ $# != 0 ]]; do
        [[ $1 == '--help' ]] || [[ $1 == '-h' ]] && { printf "%s\n\n%s\n" "$usage" "$usage_full"; exit; }
        [[ $1 == '--usage' ]] && { echo "$usage"; exit; }

        case "$1" in
            -l|--only-links) onlyLinks='true';;
            -f|--force-links) forceLinks='true';;
            *) echo "Unknown argument '$1'" >&2; exit 1;;
        esac
        shift
    done
}

main "$@"
