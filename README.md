## Setup
 ```sh
git clone --recurse-submodules --branch local git@github.com:sellithy/dotsNbin.git && cd dotsNbin && ./supportScripts/setup.sh
```
You might wanna change the branch to something other than 'local'. It's a good default though

## Notes
- If an exported env variable for one of my scripts is a file or a dir,
  follow this format `VAR_NAME_FILE_PATH` or `VAR_NAME_DIR_PATH`
- Each script should be self-contained. i.e. No common util script.

## Tips
```sh
./supportScripts/allBranches.bash diff --name-status @{push}
```

```sh
./supportScripts/allBranches.bash push -f
```

## TODO
Semi ordered by priority
- Reorganize dir structure. Put resources out of bin
- Add a standard intro to all scripts
    - `[--help | -h]` that displays usage. Make sure to check if 1st arg is == '--help' or starts with '-h'
    - Use `$(basename $0)` to get the script name
    - Change all completions to use the scripts --help option
- Add a mechanism to check for updates in this repo and notify the user
