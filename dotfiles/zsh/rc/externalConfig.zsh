# Dependencies
eval "$(/opt/homebrew/bin/brew shellenv)"
source $HOMEBREW_PREFIX/opt/zsh-syntax-highlighting/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOMEBREW_PREFIX/opt/fzf/shell/key-bindings.zsh

sdk () {
    [[ -z $SDKMAN_LOADED ]] && {
        export SDKMAN_DIR="$HOME/.sdkman"
        [[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
        export SDKMAN_LOADED=1
    }
    sdk "$@"
}

# Exports
export VISUAL=nvim
export EDITOR="$VISUAL"
export DOCKER_BUILDKIT=0
export COMPOSE_DOCKER_CLI_BUILD=0
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# FZF
export FZF_DEFAULT_COMMAND="rg --files -. -g '!.git'"

FZF_PREVIEW="
    if   [[ -f {1} ]]; then bat {1} \$([[ -z {2} ]] || echo -H {2}) --color=always
    elif [[ -d {1} ]]; then echo -n \"=== \"; ls --color -Fd {1}; ls -lAFt --color {1} | tail -n+2
    else echo {}
    fi
"

export FZF_DEFAULT_OPTS="
    --ansi -d: --reverse --margin=1
    --preview-window '+{2}+3/3,~3'
    --bind change:top
    --bind 'ctrl-/:change-preview($FZF_PREVIEW)'
"
