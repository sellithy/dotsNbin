# Didn't keep track of sources but here are some of what I used
# https://www.masterzen.fr/2009/04/19/in-love-with-zsh-part-one
# https://thevaluable.dev/zsh-completion-guide-examples
# https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org
# https://github.com/finnurtorfa/zsh/blob/master/completion.zsh

fpath=($DOTS_N_BIN_PATH/bin/completions $fpath)
zmodload zsh/complist
autoload -Uz compinit && compinit

setopt globdots    # shows stuff that starts with .
unsetopt LIST_BEEP # https://blog.vghaisas.com/zsh-beep-sound

compdef g='git'
compdef allBranches.bash='git'

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

bindkey '^I' menu-select                                 # Tab        -> open menu
bindkey -M menuselect 'O'  vi-insert                     # Shift + O  -> filters cur comps by typing
bindkey -M menuselect '^M' .accept-line                  # Enter      -> exec current line
bindkey -M menuselect '\e' undo                          # Esc        -> cancel
bindkey -M menuselect '^I' accept-and-infer-next-history # Tab (menu) -> appends cur comp then opens completion menu again

# Control zstyles
zstyle ':completion:*' menu select  # Allows selecting. Needed for accept-and-infer-next-in-history
zstyle ':completion:*' use-cache on

# Visual zstyles
zstyle ':completion:*' group-name ''        # Groups comps by their short desc
zstyle ':completion:*' list-dirs-first true # Groups dirs and files separately _and_ lists dirs first
zstyle ':completion:*' file-sort modification

## Formats descriptions, messages, and warnings
zstyle ':completion:*:descriptions' format "%F{$MAIN_COLOR}%B-- %d --%b%f"
zstyle ':completion:*:messages'     format '%F{magenta}%d%f'
zstyle ':completion:*:warnings'     format "%F{red}no matches%f"
