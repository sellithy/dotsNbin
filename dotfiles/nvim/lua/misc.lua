local lower_saturation_Sonata_purple="#7959a6"

vim.g.mapleader=" "

vim.opt.list=true
vim.opt.foldlevelstart=99
vim.opt.signcolumn = "yes"

vim.opt.swapfile = false
vim.opt.undofile=true

vim.opt.expandtab=true
vim.opt.tabstop=4
vim.opt.softtabstop=0
vim.opt.shiftwidth=0

vim.opt.nu=true
vim.opt.relativenumber=true

vim.opt.colorcolumn="121"
vim.api.nvim_set_hl(0, "ColorColumn", {bg=lower_saturation_Sonata_purple})

vim.api.nvim_set_hl(0, "CursorColumn", {bg=lower_saturation_Sonata_purple})
vim.api.nvim_set_hl(0, "CursorLine", {bg=lower_saturation_Sonata_purple})

-- Edited from https://github.com/ThePrimeagen/init.lua/blob/master/lua/theprimeagen/init.lua
vim.api.nvim_create_autocmd({"BufWritePre"}, {
  pattern="*",
  callback=function()
    if vim.bo.filetype == "markdown" then return end
    vim.cmd [[%s/\s\+$//e]]
  end,
})
