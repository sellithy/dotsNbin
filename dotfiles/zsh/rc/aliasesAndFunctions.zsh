# Temp comment untill I learn eza
# alias l='ls -lohAFt --color'
alias z='eza -rs=modified -F=always -al --icons=always --time-style=relative --color-scale=all --git --no-user'
alias t='tmuxDev'
alias reset='exec zsh -l'
alias ping='tput bel'
alias sudo='sudo '
alias updateupgrade='brew update && brew upgrade && brew autoremove'
alias c="clear"
alias vi='vi -p'
alias vim='vim -p'
alias nvim='nvim -p'

alias -g X='| xargs '
alias -g BH='--help |& col -bx |& bat -l help'
alias -g N=' >/dev/null'
alias -g NN=' 2>/dev/null'
alias -g NNN=' &>/dev/null'

g () { if [[ -z $1 ]]; then git summary; else git "$@"; fi }
