require("misc")
require("maps")
require("tiling")
require("file_specific")
require("commands")
require("lazyy") -- Can't name it lazy because it'll conflict with lazy itself
