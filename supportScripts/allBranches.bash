#!/usr/bin/env bash
set -e

git branch | awk '{print substr($0, 3, length($0))}' | while read -r line; do
    echo "-- $line --"
    git switch -q "$line"
    git "$@"
    echo
done
