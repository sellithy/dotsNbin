vim.keymap.set("n", "<leader>p", ":w<bar> exe 'silent !tmux send -t {down-of} UP Enter'<bar> redraw!<C-M>", {silent=true})
vim.api.nvim_create_autocmd("FileType", {pattern="lua", callback=function()
  vim.keymap.set("n", "<leader>p", ":w<bar> :source %<CR>", {buffer=true, silent=true})
end})

vim.keymap.set("", "0", "^")
vim.keymap.set("", "^", "0")
vim.keymap.set("", "ZZ", "<nop>")

vim.keymap.set("n", "<C-C>", ":noh<CR>", {silent=true})
vim.keymap.set("n", "<ESC>", ":noh<CR>", {silent=true})
