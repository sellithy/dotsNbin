KEYTIMEOUT=2

# region Prompt
setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
precmd() { vcs_info }

export MAIN_COLOR='cyan'
export SEC_COLOR='blue'
prompt="%F{$MAIN_COLOR}@local%f%F{white}\$vcs_info_msg_0_%f %(?..%F{red}%?%f )%F{$SEC_COLOR}%~"$'\n'">%f"
# endregion

# region bindkeys
bindkey -v

autoload -U edit-command-line && zle -N edit-command-line
bindkey '^x^e' edit-command-line
bindkey -M vicmd '^x^e' edit-command-line

# Source: https://superuser.com/a/585004
autoload -U up-line-or-beginning-search && zle -N up-line-or-beginning-search
autoload -U down-line-or-beginning-search && zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
# endregion

# region change cursor
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] || [[ $1 = 'block' ]]; then
      echo -ne '\e[0 q'

  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[6 q'
  fi
}
zle -N zle-keymap-select

_fix_cursor() {
   echo -ne '\e[6 q'
}

precmd_functions+=(_fix_cursor)
# endregion

# region History
HISTFILE="$HOME/.zsh_history"
SAVEHIST=100000000
HISTSIZE=$SAVEHIST

setopt HIST_NO_STORE
setopt HIST_IGNORE_ALL_DUPS
setopt INC_APPEND_HISTORY
# endregion
