let s:quickFixWindow=0
let s:mainWindow=0
let s:otherWindow=0

function! gitReview#startReview(whatToDiff = "")
    let l:commandArgs='--name-status ' . a:whatToDiff . ' '

    if empty(system('git diff ' . l:commandArgs))
        echohl WarningMsg | echo "Nothing to review" | echohl None
        return
    endif

    0tabnew
    call s:setupQuickFix('difftool ' . l:commandArgs)
    call s:showDiff()
endfunction

" Pre: Tab only has one window and doesn't need to be saved
" Post: Tab will have only one window which is QF with mappings setup properly
function! s:setupQuickFix(command)
    exec 'Git! ' a:command
    q

    let s:quickFixWindow=win_getid()
    nnoremap <buffer><CR> :call <SID>closeThenOpen()<CR>
    exec 'nnoremap <buffer><silent><leader>r :call <SID>setupQuickFix(' .. a:command .. ')<CR>'
endfunction

function! s:closeThenOpen()
    if ! s:closeOrError()
        return
    endif
    call s:showDiff()
endfunction

function! s:closeThenOpen()
    if ! s:closeOrError()
        return
    endif
    call s:showDiff()
endfunction

" Pre:  QF and the two diff windows are open
" Post: If either file is not saved, echos an error and returns.
"       Otherwise, closes both files
" Ret:  True if the windows were closed or there were no windows to close
function! s:closeOrError()
    if win_id2win(s:mainWindow)
        call win_gotoid(s:mainWindow)
        if getbufinfo('%')[0].changed
            echoerr "File on disk is not saved"
            return 0
        endif
    endif

    if win_id2win(s:otherWindow)
        call win_gotoid(s:otherWindow)
        if getbufinfo('%')[0].changed
            echoerr "Staged file is not saved"
            return 0
        endif
    endif

    if win_id2win(s:mainWindow)
        call win_gotoid(s:mainWindow)
        q
    endif

    if win_id2win(s:otherWindow)
        call win_gotoid(s:otherWindow)
        q
    endif

    return 1
endfunction

" Pre: Only the QF is open
" Post: File under cursor and its diff are opened but cursor remains in QF
function! s:showDiff()
    vertical .cc
    let s:mainWindow=win_getid()
    exec 'vert resize ' &columns * 0.75

    Gdiffsplit
    let s:otherWindow=win_getid()

    call win_gotoid(s:quickFixWindow)
endfunction

