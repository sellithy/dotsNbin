return {
  "neoclide/coc.nvim",
  build=":call coc#util#install()",
  lazy=false,
  keys={
    {"<TAB>", "coc#pum#visible() ? coc#pum#confirm() : '<Tab>'", mode='i', silent=true, expr=true},
    {"<leader>2",  "<Plug>(coc-diagnostic-next)", silent=true},
    {"<leader>jd", "<Plug>(coc-definition)", silent=true},
    {"<leader>jt", "<Plug>(coc-type-definition)", silent=true},
    {"<leader>ju", "<Plug>(coc-references)", silent=true},
    {"<leader>rn", "<Plug>(coc-rename)", silent=true},
    {"<leader>rr", "<Plug>(coc-codeaction-refactor)", silent=true},
    {"<leader>r", "<Plug>(coc-codeaction-refactor-selected)", silent=true, mode='x'},
    {"<leader><cr>", "<Plug>(coc-codeaction)", silent=true},
    {"<leader>f", "<Plug>(coc-format)", silent=true},
    {"<leader>f", "<Plug>(coc-format-selected)", silent=true, nowait=true, mode='x'},
    {"<leader>h", "<cmd>call CocAction('definitionHover')<CR>", silent=true},
  },
  config=function()
    -- coc-texlab
    vim.api.nvim_create_autocmd("FileType", {pattern="tex", callback=function()
      vim.keymap.set("n", "<leader>p", ":w<bar> :CocCommand latex.Build<CR>", {buffer=true, silent=true})
    end})
  end
}
