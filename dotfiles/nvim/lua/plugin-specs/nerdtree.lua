return {
  {
    "preservim/nerdtree",
    init=function()
      vim.g.NERDTreeWinSize='40%'
    end,
    keys={
      {"<leader>q", "<cmd>NERDTreeToggle<CR>"},
      {"<leader>Q", "<cmd>NERDTreeFind<CR>"},
    },
  },
  {
    "ryanoasis/vim-devicons"
  },
  {
    "PhilRunninger/nerdtree-visual-selection"
  },
}
