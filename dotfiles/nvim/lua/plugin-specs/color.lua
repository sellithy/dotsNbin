return {
  "ellisonleao/gruvbox.nvim",
  name="gruvbox",
  priority=1000,
  config=function()
    require("gruvbox").setup({
      italic = {
        strings = false,
      },
      contrast = "soft"
    })
    vim.cmd [[colorscheme gruvbox]]
    vim.api.nvim_set_hl(0, "ColorColumn", {bg="maroon"})

    vim.api.nvim_set_hl(0, "CursorColumn", {bg="#7959a6"})
    vim.api.nvim_set_hl(0, "CursorLine", {bg="#7959a6"})
  end
}
