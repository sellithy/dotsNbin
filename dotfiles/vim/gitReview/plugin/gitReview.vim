if exists('g:loaded_gitReview')
    finish
endif
" TODO uncomment when done testing
" let g:loaded_gitReview=1

command! -nargs=? GitReview :call gitReview#startReview(<q-args>)
