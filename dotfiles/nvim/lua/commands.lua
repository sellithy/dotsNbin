-- Source: Me but got idea from a video somewhere
vim.api.nvim_create_user_command('Presenter',function()
  vim.o.cursorcolumn = not(vim.o.cursorcolumn)
  vim.o.cursorline = not(vim.o.cursorline)
end,{})

-- Source: Me
local autosave_group = vim.api.nvim_create_augroup("AutoSaveGroup", {})
vim.api.nvim_create_user_command("AutoSave", function(opts)
  if opts.bang then
    vim.o.updatetime = 4000
    vim.api.nvim_clear_autocmds({group=autosave_group})
  else
    vim.o.updatetime = 250
    vim.api.nvim_create_autocmd("CursorHold", {command="update", group=autosave_group})
  end
end,{bang=true})


-- Reload file if changed on disk
-- https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#3830
local autoreload_group = vim.api.nvim_create_augroup("AutoReloadGroup", {})
vim.api.nvim_create_user_command("AutoReload", function(opts)
  if opts.bang then
    vim.o.updatetime = 4000
    vim.api.nvim_clear_autocmds({group=autoreload_group})
  else
    vim.o.updatetime = 250
    vim.api.nvim_create_autocmd({'FocusGained', 'BufEnter', 'CursorHold', 'CursorHoldI'}, {
      pattern = '*',
      command = "if mode() !~ '\v(c|r.?|!|t)' && getcmdwintype() == '' | checktime | endif",
      group=autoreload_group,
    })
    vim.api.nvim_create_autocmd({'FileChangedShellPost'}, {
      pattern = '*',
      command = "echohl WarningMsg | echo 'File changed on disk. Buffer reloaded.' | echohl None",
      group=autoreload_group,
    })
  end
end,{bang=true})
