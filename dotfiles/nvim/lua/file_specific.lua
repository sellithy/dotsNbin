vim.api.nvim_create_autocmd("FileType", {pattern="gitcommit", callback=function()
  vim.opt_local.spell=true
  vim.opt_local.textwidth=80
  vim.opt_local.colorcolumn="+1,-7"
end})

vim.api.nvim_create_autocmd("FileType", {pattern="markdown", callback=function()
  vim.opt_local.spell=true
end})

vim.api.nvim_create_autocmd("FileType", {
  pattern={"typescript", "javascript", "lua", "json"},
  callback=function()
    vim.opt_local.ts=2
  end
})
