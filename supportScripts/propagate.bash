#!/usr/bin/env bash

# Rebases the tail of a branch on the second argument
#
# First arg is a <branch> acceptable to git switch <branch>
# Second arg is <newbase> acceptable to git rebase <newbase>
#
# To find the tail of a branch, the function looks for a exactly 1 commit with
# a subject that is exactly the name of the branch. Nothing less nothing more.
rebaseTailOnTip () {
    tailCommit=$(git log --format='%H %s' "$1" | perl -ne "/^(\w*) ${1//\//\\/}$/ && print \"\$1\n\"")
    [[ -z $tailCommit ]] && { echo "Couldn't find tail for $1" >&2; return 1; }
    [[ $(wc -l <<< "$tailCommit") -ne 1 ]] && { printf "Ambiguous tails for %s:\n%s\n" "$1" "$tailCommit">&2; return 1; }

    git -c advice.skippedCherryPicks=false rebase -q --rerere-autoupdate \
        --onto "$2" "$tailCommit"~1 "$1" || git rebase --continue &>/dev/null
}

set -e

rebaseTailOnTip 'wsl' 'main'
rebaseTailOnTip 'local' 'main'
rebaseTailOnTip 'mac' 'local'
